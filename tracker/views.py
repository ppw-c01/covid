from django.shortcuts import render, redirect
import requests
import json
from . import forms, models
from django.views.decorators.csrf import csrf_exempt

# Create your views here


def index(request):
    response = requests.get(
        'https://services5.arcgis.com/VS6HdKS0VfIhv8Ct/arcgis/rest/services/COVID19_Indonesia_per_Provinsi/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json')
    covid_data = response.json()
    covid_data['form'] = forms.Provinces()
    covid_data['create_form'] = forms.createProvince()
    data_provinsi = models.Provinsi.objects.all()

    positif = 0
    sembuh = 0
    meninggal = 0

    for i in covid_data['features']:
        positif += i['attributes']['Kasus_Posi']
        sembuh += i['attributes']['Kasus_Semb']
        meninggal += i['attributes']['Kasus_Meni']
    covid_data['features'][34] = {'attributes': {'Provinsi': 'Indonesia', 'Kasus_Posi': positif,
                                                 'Kasus_Semb': sembuh, 'Kasus_Meni': meninggal}}
    if len(data_provinsi) != 0:
        for i in data_provinsi:
            data = {'attributes': {'Provinsi': i.nama, 'Kasus_Posi': i.kasus_positif,
                                   'Kasus_Semb': i.kasus_sembuh, 'Kasus_Meni': i.kasus_meninggal}}
            covid_data['features'].append(data)
    if request.method == "POST":
        if 'nama_provinsi' in request.POST:
            covid_data['features'] = search(request, covid_data)
    return render(request, 'tracker/index.html', covid_data)


def create_page(request):
    data = {
        'create_form': forms.createProvince()}
    if request.method == "POST":
        activity_form = forms.createProvince(request.POST)
        if activity_form.is_valid():
            provinsi = models.Provinsi()
            provinsi.nama = activity_form.cleaned_data['nama']
            provinsi.kasus_positif = activity_form.cleaned_data['kasus_positif']
            provinsi.kasus_sembuh = activity_form.cleaned_data['kasus_sembuh']
            provinsi.kasus_meninggal = activity_form.cleaned_data['kasus_meninggal']
            provinsi.save()
        return redirect('/tracker/')
    return render(request, 'tracker/create.html', data)


def search(request, covid_data):
    target = forms.Provinces(request.POST)
    data = []
    if target.is_valid():
        nama = target.cleaned_data.get("nama_provinsi")
        for i in covid_data['features']:
            nama_provinsi = i['attributes']['Provinsi']
            if nama in nama_provinsi:
                data.append(i)
    return data
