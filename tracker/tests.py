from django.test import TestCase, Client
import requests
from .models import Provinsi

# Create your tests here.

# TODO: Tests for forms


class testTracker(TestCase):
    def setUp(self):
        Provinsi.objects.create(
            nama="Cute-cute Land", kasus_positif="10", kasus_sembuh="10", kasus_meninggal="0")

    def test_url_index(self):
        response = Client().get('/tracker/')
        self.assertEqual(response.status_code, 200)

    def test_template_index(self):
        response = Client().get('/tracker/')
        self.assertTemplateUsed(response, 'tracker/index.html')

    def test_API(self):
        response = requests.get(
            'https://services5.arcgis.com/VS6HdKS0VfIhv8Ct/arcgis/rest/services/COVID19_Indonesia_per_Provinsi/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json')
        self.assertEqual(response.status_code, 200)

    def test_search_province(self):
        response = Client().post('/tracker/', data={'nama_provinsi': 'Aceh'})
        self.assertEqual(response.status_code, 200)

    def test_add_province_model(self):
        Provinsi.objects.create(
            nama="Moe-Moe Island", kasus_positif="10", kasus_sembuh="10", kasus_meninggal="0")
        count = Provinsi.objects.all().count()
        self.assertEqual(count, 2)

    def test_url_create_page(self):
        response = Client().get('/tracker/create_page')
        self.assertEqual(response.status_code, 200)

    def test_template_create_page(self):
        response = Client().get('/tracker/create_page')
        self.assertTemplateUsed(response, 'tracker/create.html')

    def test_add_provinsi(self):
        provinsi_num = Provinsi.objects.all().count()
        response = Client().post('/tracker/create_page',
                                 data={"nama": "Doki-doki Kuni", "kasus_positif": "1", "kasus_sembuh": "100", "kasus_meninggal": "0"})
        participant = Provinsi.objects.get(
            nama='Doki-doki Kuni')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Provinsi.objects.all().count(), provinsi_num+1)
