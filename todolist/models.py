from django.db import models

class things_list(models.Model):
    nama_list = models.CharField(max_length=20)
    deskripsi = models.CharField(max_length=150)

    todo1 = models.CharField(max_length=50)
    todo2 = models.CharField(max_length=50)
    todo3 = models.CharField(max_length=50)
    todo4 = models.CharField(max_length=50)
    todo5 = models.CharField(max_length=50)

    tobring1 = models.CharField(max_length=50)
    tobring2 = models.CharField(max_length=50)
    tobring3 = models.CharField(max_length=50)
    tobring4 = models.CharField(max_length=50)
    tobring5 = models.CharField(max_length=50)

    def __str__(self):
        return self.nama_list
