from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import SuspectForm, SearchForm
from .models import Suspect

response = {}
# Create your views here.


def index(request):
    suspects = Suspect.objects.all()
    response['suspects'] = suspects
    response['search'] = SearchForm()
    response['toggle'] = "collapse"
    if (request.method == "POST" and 'nama_suspek' in request.POST):
        target = SearchForm(request.POST)
        if (target.is_valid()):
            data_target = target.cleaned_data
            data = []
            for suspect in suspects:
                if data_target['nama_suspek'].lower() in suspect.nama_suspek.lower():
                    data.append(suspect)
            response['suspects'] = data
            response['toggle'] = "collapse show"
    return render(request, 'report/index.html', response)


def new(request):
    response = {'inputForm': SuspectForm}
    return render(request, 'report/form.html', response)


def save(request):
    form = SuspectForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        data_form = form.cleaned_data
        data_input = Suspect()
        data_input.nama_suspek = data_form['nama_suspek']
        data_input.alamat = data_form['alamat']
        data_input.tanggal_lahir = data_form['tanggal_lahir']
        data_input.status = "Not Confirmed"
        data_input.save()
    return HttpResponseRedirect('/report/')
