from django.test import TestCase, Client
from .models import Suspect
import datetime

# Create your tests here.
class testReport(TestCase):
    def setUp(self):
        Suspect.objects.create(nama_suspek="Udin", alamat="Jl. Aspal, Bumi", tanggal_lahir=datetime.datetime(2001, 9, 11))

    def test_url_index(self):
        response = Client().get('/report/')
        self.assertEqual(response.status_code, 200)

    def test_url_new(self):
        response = Client().get('/report/new/')
        self.assertEqual(response.status_code, 200)

    def test_template_index(self):
        response = Client().get('/report/')
        self.assertTemplateUsed(response, 'report/index.html')

    def test_template_form(self):
        response = Client().get('/report/new/')
        self.assertTemplateUsed(response, 'report/form.html')

    def test_add_suspek_model(self):
        Suspect.objects.create(nama_suspek="Firaun", alamat="Jl. Pasir, Mesir", tanggal_lahir=datetime.datetime(1920, 7, 19))
        count = Suspect.objects.all().count()
        self.assertEqual(count, 2)