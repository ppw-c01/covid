from django.db import models

# Create your models here.


class Provinsi(models.Model):
    nama = models.CharField(max_length=30)
    kasus_positif = models.IntegerField()
    kasus_sembuh = models.IntegerField()
    kasus_meninggal = models.IntegerField()
