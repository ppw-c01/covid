from django.shortcuts import render, redirect
from .models import things_list
from .forms import todo_list

def home(request):
    items = things_list.objects.all()
    context = {'lists' : items}
    return render(request, 'todolist/index.html', context)

def form(request):
    if request.method == "POST":
        form = todo_list(request.POST)
        if form.is_valid():
            form.save()
        return redirect('todolist:home')
    else:
        form = todo_list()
        return render(request, 'todolist/form.html', {'todo_list':form})

def every_list(request, num):
    items = things_list.objects.get(id=num)
    if request.method == 'POST':
        items.delete()
        return redirect('todolist:home')
    context = {'lists': items}
    return render(request, 'todolist/perlist.html', context)
