from django import forms
from .models import Suspect

class SearchForm(forms.ModelForm):
    class Meta:
        model = Suspect
        fields = ['nama_suspek']

    name_attrs = {
        'class' : 'form-control mb-3',
		'type' : 'text',
		'placeholder' : 'Search by name',
        'name' : 'nama_suspek',
	}

    nama_suspek = forms.CharField(label='', required= True, max_length=50, widget=forms.TextInput(attrs=name_attrs))

class SuspectForm(forms.ModelForm):
    class Meta:
        model = Suspect
        fields = ['nama_suspek', 'alamat', 'tanggal_lahir']

    name_attrs = {
        'class' : 'form-control mb-3',
		'type' : 'text',
		'placeholder' : 'Enter the name',
        'name' : 'nama_suspek',
	}

    address_attrs = {
        'class' : 'form-control mb-3',
		'type' : 'text',
		'placeholder' : 'Address',
        'name' : 'alamat',
	}

    birthdate_attrs = {
        'class' : 'form-control mb-3',
        'type' : 'date',
        'placeholder' : 'Date of Birth',
        'name' : 'tanggal_lahir',
    }

    selfpict_attrs = {
        'class' : 'form-control mb-3',
        'type' : 'file',
        'value' : 'Upload photo',
        'name' : 'foto_diri',
    }

    idpict_attrs = {
        'class' : 'form-control mb-3',
        'type' : 'file',
        'value' : 'Upload identity card',
        'name' : 'foto_ktp',
    }

    nama_suspek = forms.CharField(label='', required= True, max_length=50, widget=forms.TextInput(attrs=name_attrs))
    alamat = forms.CharField(label='Alamat', required= True, max_length=200, widget=forms.TextInput(attrs=address_attrs))
    tanggal_lahir = forms.DateField(label='Tanggal Lahir', required= True, widget=forms.TextInput(attrs=birthdate_attrs))
    #foto_diri = forms.ImageField(label='Foto Diri', required= True, widget=forms.TextInput(attrs=selfpict_attrs))
    #foto_ktp = forms.ImageField(label='Foto KTP', required= True, widget=forms.TextInput(attrs=idpict_attrs))
