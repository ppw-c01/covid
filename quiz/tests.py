from django.test import TestCase, Client

# Create your tests here.
class testQuiz(TestCase):
    def test_url_index(self):
        response = Client().get('/quiz/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_quiz(self):
        response = Client().get('/quiz/start/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_result(self):
        response = Client().get('/quiz/result/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_index(self):
        response = Client().get('/quiz/')
        self.assertTemplateUsed(response, 'quiz/index.html')
    
    def test_template_quiz(self):
        response = Client().get('/quiz/start/')
        self.assertTemplateUsed(response, 'quiz/quiz.html')
    
    """ def test_template_result(self):
        response = Client().get('/quiz/')
        self.assertTemplateUsed(response, 'quiz/resultG.html') """
    
    